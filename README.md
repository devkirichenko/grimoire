Initially, this project was created to replicate 5etools, but for Pathfinder 2 system. In my opinion, both Archives of Nethys and easytools.es, while both being a good for the playerbase, are inferior in the way they treat database view. For me, as a DM and player, its far more convenient to be able to:

1) See every DB element in a tight, sorted table

2) Being able to click on an element to get a detailed description and hidden fields of selected element

3) To filter and sort the table on the fly, without page reloading or typing anything in the search bar

4) A bonus would be awesome search engine implemented in pf2.easytools.es


As I worked on this project - so did grew my understanding of innate limitations I have to overcome to pursue this goal. I'm still trying to do this mostly in Java, but without JS it would be hard to implement some of the most wanted features.

At this moment, my APP looks like this.
![This](/screenshot.png)

It's still far from what I want it to be, but it already has the basic functionality. It is able to generate table of entities from the SQL DB (with Postgre SQL), to add new elements and delete existing ones, it has a robust (but not perfect) search and it is capable of editing elements of the table.

As far as I understand, in the future I have a simple choice:

a) I can turn to the dark side and use JavaScript to implement hyperlinked table view like here https://5e.tools/spells.html

OR

b) Use Vaadin framework and do the same, but using pure Java


And honestly, I'm not sure yet what I'll do. But here's a list of things to do before I start making things complicated:

1) Catch errors in addSpell() and editSpell() when user is trying to add element to the DB with empty name field

2) Catch errors in addSpell() and editSpell() when user is trying to put the whole Batman: The Dark Knight script into varying char field (255 symbols max)