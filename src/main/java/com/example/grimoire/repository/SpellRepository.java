package com.example.grimoire.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.example.grimoire.entity.Spell;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface SpellRepository extends CrudRepository<Spell, Integer> {
    List<Spell> findByName(String name);
    List<Spell> findByNameIgnoreCase(String name);
    Optional<Spell> findById(Integer id);

    @Query ("delete from Spell  where id=:id")          // REPLACE WITH deleteByID()
    Long removeById(@Param("id")Integer id);

    // This method only works with lowercase keywords
    @Query("select s from Spell s where lower(s.name) like %:keyword% ")
    List<Spell> findByKeywordIgnoreCase(@Param("keyword") String name);
}
