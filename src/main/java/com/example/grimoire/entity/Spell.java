package com.example.grimoire.entity;

import javax.persistence.*;


@Entity
public class Spell {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private Integer level;
    private String tradition;
    private String target;
    private String school;
    private String range;
    private String components;
    private String area;
    private String savingthrow;
    @Column(columnDefinition = "text")
    private String description;

    public Spell() {
    }

    public Spell(Integer id, String name, Integer level, String tradition, String target, String school, String range, String components, String area, String savingthrow, String description) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.tradition = tradition;
        this.target = target;
        this.school = school;
        this.range = range;
        this.components = components;
        this.area = area;
        this.savingthrow = savingthrow;
        this.description = description;
    }

    public Spell(String name, Integer level, String tradition, String target, String school, String range, String components, String area, String savingthrow, String description) {
        this.name = name;
        this.level = level;
        this.tradition = tradition;
        this.target = target;
        this.school = school;
        this.range = range;
        this.components = components;
        this.area = area;
        this.savingthrow = savingthrow;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSavingthrow() {
        return savingthrow;
    }

    public void setSavingthrow(String savingthrow) {
        this.savingthrow = savingthrow;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getTradition() {
        return tradition;
    }

    public void setTradition(String tradition) {
        this.tradition = tradition;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
