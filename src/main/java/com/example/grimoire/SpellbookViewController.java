package com.example.grimoire;

import com.example.grimoire.entity.Spell;
import com.example.grimoire.repository.SpellRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
public class SpellbookViewController {
    @Autowired
    private SpellRepository sRepo;

    @GetMapping("/spellbook")
    public String getAllSpells(Map<String, Object> model) {
        Iterable<Spell> spells = sRepo.findAll();
        model.put("spells", spells);
        return "spellbook";
    }

    @GetMapping("/additem")
    public String addEntryForm() {
        return "additem";
    }

    @PostMapping("/additem")    // localhost:8080/additem
    public String addSpell(@RequestParam(name = "name") String name,
                           @RequestParam("level") Integer level,
                           @RequestParam("school") String school,
                           @RequestParam("range") String range,
                           @RequestParam("target") String target,
                           @RequestParam("components") String components,
                           @RequestParam("area") String area,
                           @RequestParam("savingthrow") String savingthrow,
                           @RequestParam("description") String description,
                           @RequestParam("tradition") String tradition,
                           Map<String, Object> model) {
        Spell n = new Spell(name, level, tradition, target, school, range, components, area, savingthrow, description);
        sRepo.save(n);
        this.getAllSpells(model);
        return "spellbook";
    }

    @RequestMapping("/filterspells")
    public String filterSpells(@RequestParam String keyword, Map<String, Object> model) {
        Iterable<Spell> spells;
        if (keyword != null && !keyword.isEmpty()) {
            keyword = keyword.toLowerCase();
            spells = sRepo.findByKeywordIgnoreCase(keyword);
        } else {
            spells = sRepo.findAll();
        }
        model.put("spells", spells);
        return "spellbook";
    }

    @RequestMapping("deleteitem")
    public String deleteSpell(@RequestParam("id") Integer id, Map<String, Object> model) {
        Spell selector = sRepo.findById(id).get();
        sRepo.delete(selector);
        model.put("spells", sRepo);
        this.getAllSpells(model);
        return "spellbook";
    }

    @GetMapping("/edititem")
    public String editItemForm(@RequestParam("id") Integer id, ModelMap model) {
        Spell selector = sRepo.findById(id).get();
        model.put("spell", selector);
        return "edititem";
    }

    @PostMapping("/edititem")       // DELETE ID ASAP
    public String editSpell(@RequestParam("id") Integer id,
                            @RequestParam("name") String name,
                            @RequestParam("level") Integer level,
                            @RequestParam("school") String school,
                            @RequestParam("range") String range,
                            @RequestParam("target") String target,
                            @RequestParam("components") String components,
                            @RequestParam("area") String area,
                            @RequestParam("savingthrow") String savingthrow,
                            @RequestParam("description") String description,
                            @RequestParam("tradition") String tradition,
                            Map<String, Object> model) {
        Spell n = new Spell(id, name, level, tradition, target, school, range, components, area, savingthrow, description);
        sRepo.save(n);
        this.getAllSpells(model);
        return "spellbook";
    }
}